import Contact.Contact;
import Contact.PhoneNumber;
import java.util.ArrayList;
import java.util.Random;
import java.util.function.Predicate;

// Press ⇧ twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void addRandomNumber(ArrayList<Integer> list) {
        int originalSize = list.size();
        Random random = new Random();
        while(originalSize + 1 != list.size()) {
            int n = random.nextInt(1000);
            list.add(n);
        }
    }
    public static void main(String[] args) {

        // Run time Polymorphism
        //method overriding
        ConditionArrayList oddListy = new ConditionArrayList(n -> Math.abs(n) % 2 == 1);
        oddListy.add(1);
        oddListy.add(2);
        System.out.println(oddListy);

        ConditionArrayList evenListy = new ConditionArrayList(n -> Math.abs(n) % 2 == 0);
        evenListy.add(1);
        evenListy.add(2);
        System.out.println(evenListy);

        ArrayList<Integer> listy = new ArrayList<>();
        listy.add(1);
        listy.add(2);
        System.out.println(listy);

        //compile Time Polymorphism
        //more than one method with same name
        // method overloading

        Predicate<Integer> isDivisibleByThree = n -> Math.abs(n) % 3 == 0;
        ConditionArrayList divisibleByThreeListy =
                new ConditionArrayList(isDivisibleByThree);

        ConditionArrayList divisibleByThreeListy2 =
                new ConditionArrayList(isDivisibleByThree,
                        1, 2, 3, 4, 5, 6, 9);

        ArrayList<Integer> numsList = new ArrayList<>();
        numsList.add(1);
        numsList.add(4);
        numsList.add(3);
        numsList.add(6);
        numsList.add(13);

        ConditionArrayList divisibleByThreeListy3 =
                new ConditionArrayList(isDivisibleByThree, numsList);

        Predicate<Integer> isDivisbleBySix = n -> Math.abs(n) % 6 == 0;
        ConditionArrayList divisibleBySixListy = new ConditionArrayList(isDivisbleBySix, divisibleByThreeListy);

    // Contact module

        Contact contact1 = new Contact("silly", new PhoneNumber("123456789856"),"silly@gmail.com");
        Contact contact2 = new Contact("billy", new PhoneNumber("876765456"),"billy@gmail.com");
        Contact contact3 = new Contact(new PhoneNumber("4534567654"),"nulon");
        Contact contact4 = new Contact("kamal", "kamal@gmail.com");

        System.out.println(contact1);
        System.out.println(contact1);
        System.out.println(contact1);
        System.out.println(contact1);

    }
}