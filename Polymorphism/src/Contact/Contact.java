package Contact;

public class Contact {
    private String email;
    private PhoneNumber phone;
    private String name;

    public Contact(String email, PhoneNumber phone, String name) {
        this.email = email;
        this.phone = phone;
        this.name = name;
    }

    public Contact(PhoneNumber phone, String name) {
        this.phone = phone;
        this.name = name;
    }

    public Contact(String name, String email){
        this.name = name;
        this.email = email;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "email='" + email + '\'' +
                ", phone=" + phone +
                ", name='" + name + '\'' +
                '}';
    }
}
