import ModArray.ModArrayList;

import java.util.Stack;

// Press ⇧ twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args){
        Stack<Character> charStack = new Stack<>();

        charStack.push('c');
        charStack.push('a');
        charStack.push('t');

        System.out.println(charStack.pop());
        System.out.println(charStack.pop());
        System.out.println(charStack.pop());


        /* Mod Array List */

        ModArrayList modArrayList = new ModArrayList();
        modArrayList.add(0);
        modArrayList.add(10);
        modArrayList.add(20);
        modArrayList.add(30);

        System.out.println(modArrayList.getUsingMod(0));
        System.out.println(modArrayList.getUsingMod(-2));
        System.out.println(modArrayList.getUsingMod(41));

    }
}