public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance){
        this.owner = owner;
        this.balance =Math.max(balance, 0);
    }

    public double getBalance() {
        return balance;
    }
    public String getOwner(){
        return owner;
    }

    public Double withdraw(double amount){
        if (amount < this.balance){
            this.balance = balance - amount;
            return amount;
        }
        return 0.0;
    }

    public Double deposit(double amount){
        if (amount > 0){
            this.balance = balance + amount;
            return amount;
        }
        return 0.0;
    }
}
