// Press ⇧ twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        BankAccount bankAccount = new BankAccount("Tom vollat", 1000);
        bankAccount.deposit(5000);
        bankAccount.withdraw(2000);
        bankAccount.deposit(100);

        System.out.println(bankAccount.getOwner());
        System.out.println(bankAccount.getBalance());
    }
}