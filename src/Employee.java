import jdk.javadoc.doclet.Taglet;

public class Employee {
    String name;
    double salary;
    int age;
    static String location = "UK";

    public Employee(String name, double salary, int age) {
        this.name = name;
        this.salary = salary;
        this.age = age;
    }

    void raiseSalary(){
        this.salary = salary * 1.5;
    }

}
