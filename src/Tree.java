import java.awt.*;

public class Tree {
    double height;
    double trunkDiameterInches;
    TreeType treeType;

    static Color TRUNK_COLOR = new Color(102,51,0);

    Tree(double height, double trunkDiameterInches, TreeType treeType){
        this.height = height;
        this.trunkDiameterInches = trunkDiameterInches;
        this.treeType = treeType;
    }

    void grow(){
        this.height = this.height + 10;
        this.trunkDiameterInches = this.trunkDiameterInches + 1;

    }

    void announcedTallTree(){
        if(this.height > 100){
            System.out.println("This is a tall " + this.treeType + " tree");
        }
    }

    static void announcedTree(){
        System.out.println("Look out the tree " + TRUNK_COLOR);
    }

}
