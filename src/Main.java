import java.awt.*;
import java.util.concurrent.Callable;

// Press ⇧ twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Tree myFavariteOkatree = new Tree(120 , 5, TreeType.OKA);
        myFavariteOkatree.announcedTallTree();

        Tree myFavariteMapaletree = new Tree(109, 3, TreeType.MAPLE);
        myFavariteMapaletree.announcedTallTree();

        System.out.println(Tree.TRUNK_COLOR);
        Tree.announcedTree();

        Color myDefaultBlue = Color.BLUE;
        Color myDefaultWhite = Color.BLUE;

        Color brightBlue = myDefaultBlue.brighter();

        Employee employee = new Employee("Nimal", 15000, 35);
        Employee employee1 = new Employee("Kamal", 20000, 30);
        employee.raiseSalary();
        System.out.println(employee.salary);
        System.out.println(employee1.salary);

    }
}